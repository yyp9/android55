package com.example.photos;


import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;

class Photo implements Serializable {
    public ArrayList<String> locationTags = new ArrayList<String>();;
    public ArrayList<String> personTags = new ArrayList<String>();;
    public ArrayList<String> Tags = new ArrayList<String>();
    public SerializedImg currImg;
    public String location;
//    public String sUri;
    public Photo(String loc, SerializedImg currP) {
        locationTags.add("");
        Tags.add("");
        personTags.add("");


//        Tags = new ArrayList<String>();
        location = "" + loc;
//        sUri = phoUri;
        currImg = currP;
//        locationTags = new ArrayList<String>();
//        personTags = new ArrayList<String>();
    }
}

class Album implements Serializable{
    public ArrayList<Photo> photoList;
    public ArrayList<String> caps;
    public String albumName;

    public Album(String name) {
        albumName = name;
        photoList = new ArrayList<Photo>();
        caps = new ArrayList<String>();
    }

    public String toString(){
        return albumName;
    }
}


public class Photos extends AppCompatActivity implements Serializable{
    private EditText newAlbum;
    private Button addButton;
    private ListView albumList;
    private Button searchButton;
    private ArrayAdapter<Album> albumAdapter;
    ArrayList<Album> allAlbums;
    public static final String ALL_ALBUMS = "all_albums";
    public static final String SEARCH = "search_albums";
    ActivityResultLauncher<Intent> activityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if(result.getResultCode() == 3){
                        Intent data = result.getData();
                        Bundle bundle = data.getExtras();
                        if(bundle == null){
                            return;
                        }
                        int ind = bundle.getInt(AlbumOptions.ALBUM_INDEX);
                        allAlbums.remove(ind);
                        albumAdapter.notifyDataSetChanged();
                        serialize();

                    }else if(result.getResultCode() == 2){
                        Intent data = result.getData();
                        Bundle b = data.getExtras();
//                        Album c = (Album) data.getSerializableExtra(AlbumOptions.CURRENT_ALBUM);
                        allAlbums.get(b.getInt(AlbumOptions.ALBUM_INDEX)).albumName = b.getString(AlbumOptions.NEW_NAME);
//                        allAlbums.set(b.getInt(AlbumOptions.ALBUM_INDEX), c);
                        albumAdapter.notifyDataSetChanged();
                        serialize();
                    }
                }
            });
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.albums_list);

        newAlbum = (EditText)findViewById(R.id.add_new_field);
        addButton = (Button)findViewById(R.id.add_button);
        searchButton = (Button)findViewById(R.id.search_button);

        albumList = (ListView)findViewById(R.id.album_list);

        allAlbums = new ArrayList<Album>();
        deserialize();
        albumAdapter = new ArrayAdapter<Album>(this, android.R.layout.simple_list_item_1, allAlbums);
        albumList.setAdapter(albumAdapter);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();

                Intent I = new Intent(Photos.this, PhotoSearch.class);
//                I.putExtra(SEARCH, allAlbums);
                startActivity(I);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                boolean dup = false;
                for(int i = 0; i < allAlbums.size(); i++){
                    if(newAlbum.getText().toString().equals(allAlbums.get(i).toString())){
                        dup = true;
                        break;
                    }
                }
                if(dup == true){
                    Toast.makeText(Photos.this, "Album with that name already exists", Toast.LENGTH_LONG).show();
                }else if(newAlbum.getText().toString().length() < 1){
                    Toast.makeText(Photos.this, "Enter a Valid album name", Toast.LENGTH_LONG).show();
                }
                else{
                    allAlbums.add(new Album(newAlbum.getText().toString()));
                    albumAdapter.notifyDataSetChanged();
                }

                serialize();
            }
        });

        albumList.setOnItemClickListener((p, V, pos, id) -> albumOptions(pos));

    }

    public void serialize(){
        Context context = this;
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("data.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(allAlbums);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserialize(){
        Context context = this;
        try {
            FileInputStream fileInputStream = context.openFileInput("data.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            allAlbums = (ArrayList<Album>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void albumOptions(int pos) {
        Bundle b = new Bundle();
        b.putInt(AlbumOptions.ALBUM_INDEX, pos);
        Intent i = new Intent(this, AlbumOptions.class);
//        i.putExtra(AlbumOptions.CURRENT_ALBUM, allAlbums.get(pos));
//        i.putExtra(ALL_ALBUMS, allAlbums);
        i.putExtras(b);
        activityLauncher.launch(i);

//        Bundle b = new Bundle();
//        b.putString(AlbumOptions.ALBUM_NAME, allAlbums.get(pos).toString());
//        b.putInt(AlbumOptions.ALBUM_INDEX, pos);
//        Intent intent = new Intent(this, AlbumOptions.class);
//        intent.putExtras(b);
//        activityLauncher.launch(intent);

    }
}