package com.example.photos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Locale;

public class PhotoSearch extends AppCompatActivity {
    AutoCompleteTextView autocomplete;
    private CheckBox person;
    private CheckBox location;
    private ListView photosList;
    private ArrayList<Album> allAlbums;
    private ArrayList<String> personTags;
    private ArrayList<String> locationTags;
    private ArrayList<String> main;
    private PhotosListAdapter phAdapter;
    private ArrayList<Photo> phs;
    private int checkStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_search);
        deserialize();
//        allAlbums = (ArrayList<Album>) getIntent().getSerializableExtra(Photos.SEARCH);
        person = (CheckBox)findViewById(R.id.person);
        location = (CheckBox)findViewById(R.id.location);
        personTags = new ArrayList<String>();
        locationTags = new ArrayList<String>();
//        System.out.println(allAlbums.get(0).photoList.get(0).Tags);
        for(int i = 0; i < allAlbums.size(); i++){
            for(int j = 0; j < allAlbums.get(i).photoList.size(); j++){
                for(int k = 0; k < allAlbums.get(i).photoList.get(j).personTags.size(); k++){
                    if(!personTags.contains(allAlbums.get(i).photoList.get(j).personTags.get(k))) {
                        personTags.add(allAlbums.get(i).photoList.get(j).personTags.get(k));
                    }
                }
                for(int k = 0; k < allAlbums.get(i).photoList.get(j).locationTags.size(); k++) {
                    if(!locationTags.contains(allAlbums.get(i).photoList.get(j).locationTags.get(k))) {
                        locationTags.add(allAlbums.get(i).photoList.get(j).locationTags.get(k));
                    }
                }
            }
        }
//        System.out.println(personTags);
//        System.out.println(locationTags);
        main = new ArrayList<String>();
//        personTags.add("person");
//        locationTags.add("rutgers");

        person.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                auto();
            }
        });
        location.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                auto();
            }
        });



    }
    private void auto(){
        if (person.isChecked()) {
            main = personTags;
            checkStatus = 1;
        } else if (location.isChecked()) {
            main = locationTags;
            checkStatus = 2;
        }
        if (person.isChecked() && location.isChecked()) {
            Toast.makeText(this,"Please Select only one box",Toast.LENGTH_LONG).show();
            person.setChecked(false);
            location.setChecked(false);
        }
        autocomplete = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item, main);

        photosList = (ListView) findViewById(R.id.searchPhotos);
//        String tag = (String) photosList.getItemAtPosition(i);
        phs = new ArrayList<Photo>();
        phAdapter = new PhotosListAdapter(PhotoSearch.this, phs);
        photosList.setAdapter(phAdapter);

        autocomplete.setThreshold(1);
        autocomplete.setAdapter(adapter);
        autocomplete.setOnItemClickListener((p, V, pos, id) -> searchFor(pos));
//        autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                if(checkStatus == 1){
//                    for(int j = 0; j < allAlbums.size(); j++){
//                        for(int z = 0; z < allAlbums.get(j).photoList.size(); z++){
//                            for(int k = 0; k < allAlbums.get(j).photoList.get(z).personTags.size(); k++){
//                                if(allAlbums.get(j).photoList.get(z).personTags.get(k).contains(tag.toLowerCase(Locale.ROOT))){
//                                    phs.add(allAlbums.get(j).photoList.get(z));
//                                }
//                            }
//                        }
//                    }
//                }else if(checkStatus == 2){
//                    for(int j = 0; j < allAlbums.size(); j++){
//                        for(int z = 0; z < allAlbums.get(j).photoList.size(); z++){
//                            for(int k = 0; k < allAlbums.get(j).photoList.get(z).locationTags.size(); k++){
//                                if(allAlbums.get(j).photoList.get(z).locationTags.get(k).contains(tag.toLowerCase(Locale.ROOT))){
//                                    phs.add(allAlbums.get(j).photoList.get(z));
//                                }
//                            }
//                        }
//                    }
//                }
////                for(int j = 0; j < allAlbums.size(); j++){
////                    for(int z = 0; z < allAlbums.get(j).photoList.size(); z++){
////                        for(int k = 0; k < allAlbums.get(j).photoList.get(z).Tags.size(); k++){
////                            if(allAlbums.get(j).photoList.get(z).Tags.get(k).contains(tag)){
////                                phs.add(allAlbums.get(j).photoList.get(z));
////                            }
////                        }
////                    }
////                }
//
//
//
//            }
//        });
    }
    private void searchFor(int pos) {
        String tag = main.get(pos);
        if(checkStatus == 1){
            for(int j = 0; j < allAlbums.size(); j++){
                for(int z = 0; z < allAlbums.get(j).photoList.size(); z++){
                    for(int k = 0; k < allAlbums.get(j).photoList.get(z).personTags.size(); k++){
                        if(allAlbums.get(j).photoList.get(z).personTags.get(k).contains(tag.toLowerCase(Locale.ROOT))){
                                if(!phs.contains(allAlbums.get(j).photoList.get(z)))
                            phs.add(allAlbums.get(j).photoList.get(z));
                        }
                    }
                }
            }
        }else if(checkStatus == 2){
            for(int j = 0; j < allAlbums.size(); j++){
                for(int z = 0; z < allAlbums.get(j).photoList.size(); z++){
                    for(int k = 0; k < allAlbums.get(j).photoList.get(z).locationTags.size(); k++){
                        if(allAlbums.get(j).photoList.get(z).locationTags.get(k).contains(tag.toLowerCase(Locale.ROOT))){
                            if(!phs.contains(allAlbums.get(j).photoList.get(z)))
                                phs.add(allAlbums.get(j).photoList.get(z));
                        }
                    }
                }
            }
        }
//                for(int j = 0; j < allAlbums.size(); j++){
//                    for(int z = 0; z < allAlbums.get(j).photoList.size(); z++){
//                        for(int k = 0; k < allAlbums.get(j).photoList.get(z).Tags.size(); k++){
//                            if(allAlbums.get(j).photoList.get(z).Tags.get(k).contains(tag)){
//                                phs.add(allAlbums.get(j).photoList.get(z));
//                            }
//                        }
//                    }
//                }
    }

    public void deserialize(){
        Context context = this;
        try {
            FileInputStream fileInputStream = context.openFileInput("data.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            allAlbums = (ArrayList<Album>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}