package com.example.photos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class MovePhoto extends AppCompatActivity {

    private ListView albList;
    private Button Back;
    private ArrayList<Album> allTheAlbums;
    private ArrayAdapter<Album> albAdap;
    private int selectedAlbum;
    private int previousAlbum;
    private int presentPhoto;
    public static final String MOVE_TO_ALBUM = "move_to_album";
    public static final String prevAlbumInd = "previous_album";
    public static final String PHOTO_IND = "photo_index";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.move_photo);

        albList = findViewById(R.id.albumsList);
        Back = findViewById(R.id.cancel_album_process);

        deserialize();
        albAdap = new ArrayAdapter<Album>(this, android.R.layout.simple_list_item_1, allTheAlbums);
        albList.setAdapter(albAdap);

        previousAlbum = getIntent().getExtras().getInt(prevAlbumInd);
        presentPhoto = getIntent().getExtras().getInt(PHOTO_IND);

        albList.setOnItemClickListener((p, V, pos, id) -> moveTo(pos));
    }

    public void deserialize(){
        Context context = this;
        try {
            FileInputStream fileInputStream = context.openFileInput("data.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            allTheAlbums = (ArrayList<Album>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void moveTo(int pos){

        if(previousAlbum == pos){
            Toast.makeText(MovePhoto.this, "Select Another Album", Toast.LENGTH_LONG).show();
        }else {
            selectedAlbum = pos;
            Bundle b = new Bundle();
            b.putInt(MOVE_TO_ALBUM, pos);
            b.putInt(AlbumOptions.ALBUM_INDEX, previousAlbum);
            Intent i = new Intent();
            i.putExtras(b);
            setResult(749, i);
            finish();
        }
    }

    public void CancelProcess(View view){
        Bundle b = new Bundle();
        b.putInt(PHOTO_IND, presentPhoto);
        Intent i = new Intent(this, PhotoDisplay.class);
        i.putExtras(b);
        startActivity(i);
    }
}