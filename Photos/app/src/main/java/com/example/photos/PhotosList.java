package com.example.photos;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.pm.PackageInfoCompat;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class PhotosList extends AppCompatActivity implements Serializable {

    private Button addButton;
    private ListView photosList;
    private ArrayList<Photo> phs;
    private ArrayList<String> caps;
    private int AlbumIndex;
    public static final int SELECT_PICTURE_CODE = 4;
    public static final int PHOTO_OPTIONS_CODE = 442;
    private PhotosListAdapter phAdapter;
    private Album currAlbum;
    private ArrayList<Album> all_Albums;
    public static final String ALBU_PH = "album_photos";
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_list);
        addButton = findViewById(R.id.add_new_photo);
        photosList = (ListView) findViewById(R.id.listOfPhotos);
//        phs = (ArrayList<Photo>) getIntent().getSerializableExtra(AlbumOptions.ALL_PHOTOS);
        phs = new ArrayList<Photo>();
        AlbumIndex = getIntent().getExtras().getInt(AlbumOptions.ALBUM_INDEX);
        caps = getIntent().getExtras().getStringArrayList(AlbumOptions.ALL_PHOTOS_CAPS);
//        currAlbum = (Album) getIntent().getSerializableExtra(AlbumOptions.CURRENT_ALBUM);
//        allAlbums = (ArrayList<Album>) getIntent().getSerializableExtra(Photos.ALL_ALBUMS);
        deserialize();

        phAdapter = new PhotosListAdapter(this, phs);
        photosList.setAdapter(phAdapter);

        for(int i = 0; i < all_Albums.get(AlbumIndex).photoList.size(); i++) {
            phs.add(all_Albums.get(AlbumIndex).photoList.get(i));
            phAdapter.notifyDataSetChanged();
        }
        serialize();
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Picture Selection"), SELECT_PICTURE_CODE);

            }
        });
        photosList.setOnItemClickListener((p, V, pos, id) -> displayOptions(pos));
//        photosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                displayOptions(i-1);
//            }
//        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(requestCode == SELECT_PICTURE_CODE){
                Uri imageSelectionURI = data.getData();
                if(imageSelectionURI != null){
                    boolean dup = false;
                    for(int i = 0; i < phs.size(); i++){
                        if(phs.get(i).location.equals(imageSelectionURI.getPath())){
                            dup = true;
                            break;
                        }
                    }
                    if(dup == true){
                        Toast.makeText(this, "That photo already exists", Toast.LENGTH_LONG).show();
                    }else {
                        Bitmap bmpFormat = null;
                        try {
                            bmpFormat = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageSelectionURI);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        phs.add(new Photo(imageSelectionURI.getPath(), new SerializedImg(bmpFormat)));
                        phAdapter.notifyDataSetChanged();
                        serialize();
                    }
                }
            }
        }else if(resultCode == 10){
            Bundle bundle = data.getExtras();
            int ind = bundle.getInt(PhotoOptions.PHOTO_INDEX);
            phs.remove(ind);
            phAdapter.notifyDataSetChanged();
            serialize();
        }
    }

    private void displayOptions(int pos){
        Bundle b = new Bundle();
        b.putInt(PhotoOptions.PHOTO_INDEX, pos);
        b.putInt(AlbumOptions.ALBUM_INDEX, AlbumIndex);
        Intent i = new Intent(this, PhotoOptions.class);
        i.putExtras(b);
//        i.putExtra(ALBU_PH, phs);
//        i.putExtra(PhotoOptions.CURR_PHOTO, phs.get(pos));
//        i.putExtra(AlbumOptions.CURRENT_ALBUM, currAlbum);
        startActivityForResult(i, PHOTO_OPTIONS_CODE);
    }

    public void serialize(){
        Context context = this;
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("data.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            all_Albums.get(AlbumIndex).photoList.clear();
            for(int i = 0; i < phs.size(); i++){
                if(!all_Albums.get(AlbumIndex).photoList.contains(phs.get(i)))
                all_Albums.get(AlbumIndex).photoList.add(phs.get(i));
            }
            objectOutputStream.writeObject(all_Albums);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserialize(){
        Context context = this;
        try {
            FileInputStream fileInputStream = context.openFileInput("data.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            all_Albums = (ArrayList<Album>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}