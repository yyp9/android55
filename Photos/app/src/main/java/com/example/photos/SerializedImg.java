package com.example.photos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class SerializedImg implements Serializable {
    private byte[] imgArr;

    public SerializedImg(Bitmap b){
        ByteArrayOutputStream BAO = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, BAO);
        imgArr = BAO.toByteArray();
    }

    public Bitmap getBitMapFormat(){
        return BitmapFactory.decodeByteArray(imgArr, 0, imgArr.length);
    }
}
