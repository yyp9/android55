package com.example.photos;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.transition.Slide;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class PhotoDisplay extends AppCompatActivity implements Serializable {
    private ListView tagList;
    private Button slideShowButton;
    private Button addPersonButton;
    private ImageView dispImage;
    private Button addLocationButton;
    private Button movePhotoButton;
    private Button deleteTagButton;

    private EditText personText;
    private EditText locationText;

    private Photo cPhoto;
    private int phInd;
    private ArrayAdapter<String> tagAdapter;
    private int aLInd;
    private ArrayList<Photo> PL;
    public static final String CURR_ALB = "curr_alb";
    private ArrayList<Album> these_albums;
    public static final String SLIDESHOW_PHLIST = "slideshow_photos_list";
    private ArrayList<String> these_Tags;
    private ArrayList<String> these_peeps;
    private ArrayList<String> these_locs;
    public static final int MOVE_ALBUM_CODE = 757;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_display);

        tagList = findViewById(R.id.tagsList);
        slideShowButton = findViewById(R.id.slideshow_button);
        addPersonButton = findViewById(R.id.add_person_button);
        addLocationButton = findViewById(R.id.add_location_button);
        movePhotoButton = findViewById(R.id.move_photo_button);
        deleteTagButton = findViewById(R.id.delete_tag_button);
        personText = findViewById(R.id.person_field);
        locationText = findViewById(R.id.location_field);
        dispImage = findViewById(R.id.phImgView);


        phInd = getIntent().getExtras().getInt(PhotoOptions.PHOTO_INDEX);
        aLInd = getIntent().getExtras().getInt(AlbumOptions.ALBUM_INDEX);
//        PL = (ArrayList<Photo>) getIntent().getSerializableExtra(PhotoOptions.ALB_1);
        deserialize();

        if(phInd >= these_albums.get(aLInd).photoList.size()){
            Toast.makeText(PhotoDisplay.this, "No pictures to display.  Either moved or deleted", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, Photos.class);
            startActivity(i);
        }else {
            cPhoto = these_albums.get(aLInd).photoList.get(phInd);
        }
//        these_peeps = new ArrayList<String>();
//        these_locs = new ArrayList<String>();
//        these_Tags = new ArrayList<String>();

        tagAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cPhoto.Tags);
        tagList.setAdapter(tagAdapter);

//        for(int i = 0; i < cPhoto.Tags.size(); i++){
//            these_Tags.add(cPhoto.Tags.get(i));
//            tagAdapter.notifyDataSetChanged();
//        }
//        for(int i = 0; i < cPhoto.personTags.size(); i++){
//            these_peeps.add(cPhoto.personTags.get(i));
//        }
//        for(int i = 0; i < cPhoto.locationTags.size(); i++){
//            these_locs.add(cPhoto.locationTags.get(i));
//        }

        serialize();
//        String[] sTags = new String[cPhoto.Tags.size()];
//        CharSequence[] sTags = new CharSequence[these_Tags.size()];
//        for(int i = 0; i < sTags.length; i++){
//            sTags[i] = "" + these_Tags.get(i);
//        }
//        dispImage.setImageURI(Uri.parse(cPhoto.sUri));
        dispImage.setImageBitmap(cPhoto.currImg.getBitMapFormat());
//        AlertDialog.Builder b = new AlertDialog.Builder(this);
//        b.setMessage("Click on a Tag on the List To Delete")
//        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        })
        //;
        addPersonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pers = personText.getText().toString();
                if(pers.length() > 0){
                    String f = "Person:" + pers;
                    if(!cPhoto.Tags.contains(f)){
//                        these_Tags.add(f);
                        cPhoto.Tags.add(f);
                        cPhoto.personTags.add(pers.toLowerCase(Locale.ROOT));
                        tagAdapter.notifyDataSetChanged();
                        these_albums.get(aLInd).photoList.add(phInd, cPhoto);
                        serialize();
                    }else{
                        Toast.makeText(PhotoDisplay.this, "That Tag Already Exists", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(PhotoDisplay.this, "Enter a valid Tag Value", Toast.LENGTH_LONG).show();
                }
            }
        });
        addLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String loc = locationText.getText().toString();
                if(loc.length() > 0){
                    String l = "Location:" + loc;
                    if(!cPhoto.Tags.contains(l)){
//                        these_Tags.add(l);
                        cPhoto.Tags.add(l);
                        cPhoto.locationTags.add(loc.toLowerCase(Locale.ROOT));
                        tagAdapter.notifyDataSetChanged();
                        these_albums.get(aLInd).photoList.add(phInd, cPhoto);
                        serialize();
                    }else{
                        Toast.makeText(PhotoDisplay.this, "That Tag Already Exists", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(PhotoDisplay.this, "Enter a valid Tag Value", Toast.LENGTH_LONG).show();
                }
            }
        });
        deleteTagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(PhotoDisplay.this);
                b.setTitle("Delete Tag");
                b.setMessage("Select a tag to delete from the Tag List");
                b.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       dialogInterface.dismiss();
                    }
                });
                AlertDialog t = b.create();
                t.show();
            }
        });
        tagList.setOnItemClickListener((p, V, pos, id) -> removeTag(pos));
    }

    private void removeTag(int pos){
        String s = these_Tags.get(pos);
        if(s.charAt(0) == 'L'){
            cPhoto.locationTags.remove(s.substring(s.indexOf(":") + 1));
        }else{
            cPhoto.personTags.remove(s.substring(s.indexOf(":") + 1));
        }
        cPhoto.Tags.remove(pos);
        tagAdapter.notifyDataSetChanged();
        these_albums.get(aLInd).photoList.add(phInd, cPhoto);
        serialize();
    }

    public void displaySlideShow(View view){
        Bundle b = new Bundle();
        b.putInt(PhotoOptions.PHOTO_INDEX, phInd);
        b.putInt(AlbumOptions.ALBUM_INDEX, aLInd);
        Intent i = new Intent(this, SlideshowDisplay.class);
        i.putExtras(b);
//        i.putExtra(SLIDESHOW_PHLIST, PL);
        startActivity(i);
    }
    public void movePhoto(View view){
        Bundle b = new Bundle();
        b.putInt(MovePhoto.prevAlbumInd, aLInd);
        b.putInt(MovePhoto.PHOTO_IND, phInd);
        Intent i = new Intent(this, MovePhoto.class);
        i.putExtras(b);
        startActivityForResult(i, MOVE_ALBUM_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == 749){
            Bundle b = data.getExtras();

            int moveTo = b.getInt(MovePhoto.MOVE_TO_ALBUM);
            these_albums.get(moveTo).photoList.add(cPhoto);
            int newPhInd = these_albums.get(moveTo).photoList.size() -1;
            these_albums.get(aLInd).photoList.remove(phInd);
            serialize();
            Intent i = new Intent(this, PhotosList.class);
            Bundle bb = new Bundle();
            bb.putInt(AlbumOptions.ALBUM_INDEX, aLInd);
            i.putExtras(bb);
            startActivity(i);
//            modSerialize(moveTo, newPhInd);
        }
    }

//    public void modSerialize(int moveToInd, int newInd){
//        Context context = this;
//        try {
//            FileOutputStream fileOutputStream = context.openFileOutput("data.dat", Context.MODE_PRIVATE);
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
//            these_albums.get(moveToInd).photoList.get(newInd).Tags.clear();
//            these_albums.get(moveToInd).photoList.get(newInd).locationTags.clear();
//            these_albums.get(moveToInd).photoList.get(newInd).personTags.clear();
//
//            for(int i = 0; i < these_Tags.size(); i++){
//                these_albums.get(moveToInd).photoList.get(newInd).Tags.add(these_Tags.get(i));
//            }
//            for(int i = 0; i < these_locs.size(); i++){
//                these_albums.get(moveToInd).photoList.get(newInd).locationTags.add(these_locs.get(i));
//            }
//            for(int i = 0; i < these_peeps.size(); i++){
//                these_albums.get(moveToInd).photoList.get(newInd).personTags.add(these_peeps.get(i));
//            }
//            objectOutputStream.writeObject(these_albums);
//            objectOutputStream.close();
//            fileOutputStream.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void serialize(){
        Context context = this;
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("data.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
//            these_albums.get(aLInd).photoList.get(phInd).Tags.clear();
//            these_albums.get(aLInd).photoList.get(phInd).locationTags.clear();
//            these_albums.get(aLInd).photoList.get(phInd).personTags.clear();
//
//            for(int i = 0; i < these_Tags.size(); i++){
//                these_albums.get(aLInd).photoList.get(phInd).Tags.add(these_Tags.get(i));
//            }
//            for(int i = 0; i < these_locs.size(); i++){
//                these_albums.get(aLInd).photoList.get(phInd).locationTags.add(these_locs.get(i));
//            }
//            for(int i = 0; i < these_peeps.size(); i++){
//                these_albums.get(aLInd).photoList.get(phInd).personTags.add(these_peeps.get(i));
//            }
            objectOutputStream.writeObject(these_albums);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserialize(){
        Context context = this;
        try {
            FileInputStream fileInputStream = context.openFileInput("data.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            these_albums = (ArrayList<Album>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}