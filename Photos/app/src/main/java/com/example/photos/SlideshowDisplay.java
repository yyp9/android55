package com.example.photos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class SlideshowDisplay extends AppCompatActivity implements Serializable {

    private ImageView currImg;
    private Button nextButton;
    private Button prevButton;

    private int currPhInd;
    private int currAlbInt;
    private int currInd;
    private ArrayList<Photo> pList;

    private ArrayList<Album> all_albs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slideshow_display);

        currImg = findViewById(R.id.slideShow_imgView);
        nextButton = findViewById(R.id.next_pic);
        prevButton = findViewById(R.id.prev_pic);

        currPhInd = getIntent().getExtras().getInt(PhotoOptions.PHOTO_INDEX);
        currAlbInt = getIntent().getExtras().getInt(AlbumOptions.ALBUM_INDEX);
        deserialize();
        pList = new ArrayList<Photo>();
        for(int i = 0; i < all_albs.get(currAlbInt).photoList.size(); i++){
            pList.add(all_albs.get(currAlbInt).photoList.get(i));
        }
        currInd = currPhInd;
        if(currInd >= pList.size()){
            Toast.makeText(SlideshowDisplay.this, "No pictures to display.  Either moved or deleted", Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, Photos.class);
            startActivity(i);
        }else {
            currImg.setImageBitmap(pList.get(currInd).currImg.getBitMapFormat());
        }
//        pList = (ArrayList<Photo>) getIntent().getSerializableExtra(PhotosList.ALBU_PH);
//        if(pList != null){
//            System.out.println(true);
////            currImg.setImageURI(Uri.parse(pList.get(currInd).sUri));
//        }

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currInd == pList.size() - 1){
                    currInd = 0;
                }else{
                    currInd++;
                }
                currImg.setImageBitmap(pList.get(currInd).currImg.getBitMapFormat());

//                currImg.setImageURI(Uri.parse(pList.get(currInd).sUri));
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currInd == 0){
                    currInd = pList.size() - 1;
                }else{
                    currInd--;
                }
                currImg.setImageBitmap(pList.get(currInd).currImg.getBitMapFormat());
//                currImg.setImageURI(Uri.parse(pList.get(currInd).sUri));
            }
        });
    }

    public void deserialize(){
        Context context = this;
        try {
            FileInputStream fileInputStream = context.openFileInput("data.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            all_albs = (ArrayList<Album>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}