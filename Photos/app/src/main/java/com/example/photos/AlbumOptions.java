package com.example.photos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.ActivityResultRegistry;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.AppBarConfiguration;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;


public class AlbumOptions extends AppCompatActivity implements Serializable {


    private EditText newAlbumNameField;
    private Button rename;
    private Button delete;
    private Button open;
    public static final String ALBUM_INDEX = "album_index";
    public static final String CURRENT_ALBUM = "current_album";
    public static final String ALL_PHOTOS = "album_photos";
    public static final String ALL_PHOTOS_CAPS = "photos_captions";
    public static final String NEW_NAME = "new_name";
    private int index;
    private Album currAlbum;
    private ArrayList<Album> Albums;
//    ActivityResultLauncher<Intent> activityLauncher2 = registerForActivityResult(
//            new ActivityResultContracts.StartActivityForResult(),
//            new ActivityResultCallback<ActivityResult>() {
//                @Override
//                public void onActivityResult(ActivityResult result) {
//                    if(result.getResultCode() == 1){
//                        Intent data = result.getData();
//                        Bundle b = data.getExtras();
//                        if(b != null){
//
//                        }
//                    }
//                }
//            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_options);

        rename = findViewById(R.id.rename_button);
        delete = findViewById(R.id.delete_album_button);
        open = findViewById(R.id.open_button);
        newAlbumNameField = findViewById(R.id.new_name_field);

//            currAlbum = (Album) getIntent().getSerializableExtra(CURRENT_ALBUM);
//            allAlbums = (ArrayList<Album>) getIntent().getSerializableExtra(Photos.ALL_ALBUMS);
            deserialize();
            index = getIntent().getExtras().getInt(ALBUM_INDEX);
            newAlbumNameField.setHint(Albums.get(index).albumName);


    }

    public void renameAlbum(View view){
        String newName = newAlbumNameField.getText().toString();
        //Error condition here
        if (newName.length() < 1){
            Toast.makeText(this, "Enter a Valid Album name", Toast.LENGTH_LONG).show();
            return;
        }
        boolean dup = false;
        for(int i = 0; i < Albums.size(); i++){
            if(Albums.get(i).toString().equals(newName)){
                dup = true;
                break;
            }
        }
        if(dup == true){
            Toast.makeText(this, "Album with that name already exists", Toast.LENGTH_LONG).show();
            return;
        }
//        Albums.get(index).albumName = newName;
        Bundle b = new Bundle();
//        System.out.println(currPos);

        b.putInt(ALBUM_INDEX, index);
//        System.out.println(currPos);
        b.putString(NEW_NAME, newName);
//        b.putString(ALBUM_NAME, newName);
        serialize();
        Intent i = new Intent();
        i.putExtra(CURRENT_ALBUM, currAlbum);
        i.putExtras(b);
        setResult(2, i);
        finish();
    }

    public void deleteAlbum(View view){
        Bundle b = new Bundle();
        b.putInt(ALBUM_INDEX, index);
        serialize();
        Intent intent = new Intent();
        intent.putExtras(b);
        setResult(3, intent);
        finish();
    }

    public void openAlbum(View view){
        Bundle B = new Bundle();
        B.putInt(ALBUM_INDEX, index);
        Intent I = new Intent(this, PhotosList.class);
        I.putExtras(B);
//        I.putExtra(ALL_PHOTOS, currAlbum.photoList);
//        I.putExtra(CURRENT_ALBUM, currAlbum);
//        I.putExtra(Photos.ALL_ALBUMS, Albums);
        startActivity(I);
    }

    public void serialize(){
        Context context = this;
        try {
            FileOutputStream fileOutputStream = context.openFileOutput("data.dat", Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(Albums);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserialize(){
        Context context = this;
        try {
            FileInputStream fileInputStream = context.openFileInput("data.dat");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            Albums = (ArrayList<Album>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}