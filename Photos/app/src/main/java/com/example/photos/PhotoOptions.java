package com.example.photos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.Serializable;
import java.util.ArrayList;

public class PhotoOptions extends AppCompatActivity implements Serializable {

    private Button delButton;
    private Button dispButton;

    public static final String PHOTO_INDEX = "photo_index";
    public static final String CURR_PHOTO = "current_photo";
    public static final int DISPLAY_CODE = 33;
    private Album currAlbum;
    private int pIndex;
    private int aIndex;
    private Photo currPhoto;
    private ArrayList<Photo> currP;
    public static final String ALB_1 = "ph_alb";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_options);

        delButton = findViewById(R.id.delete_photo_button);
        dispButton = findViewById(R.id.display_photo_button);

//        currPhoto = (Photo) getIntent().getSerializableExtra(CURR_PHOTO);
        pIndex = getIntent().getExtras().getInt(PHOTO_INDEX);
        aIndex = getIntent().getExtras().getInt(AlbumOptions.ALBUM_INDEX);
//        currAlbum = (Album) getIntent().getSerializableExtra(AlbumOptions.CURRENT_ALBUM);
//        currP = (ArrayList<Photo>) getIntent().getSerializableExtra(PhotosList.ALBU_PH);

    }

    public void displayPhoto(View view){
        Bundle b = new Bundle();
        b.putInt(PHOTO_INDEX, pIndex);
        b.putInt(AlbumOptions.ALBUM_INDEX, aIndex);
        Intent i = new Intent(this, PhotoDisplay.class);
//        i.putExtra(CURR_PHOTO, currPhoto);
//        i.putExtra(ALB_1, currP);
        i.putExtras(b);
//        i.putExtra(AlbumOptions.CURRENT_ALBUM, currAlbum);
        startActivity(i);
    }
    public void deletePhoto(View view){
        Bundle b = new Bundle();
        b.putInt(PHOTO_INDEX, pIndex);
        Intent i = new Intent();
        i.putExtras(b);
        setResult(10, i);
        finish();
    }
}