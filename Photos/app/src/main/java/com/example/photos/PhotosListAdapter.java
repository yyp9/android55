package com.example.photos;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;


public class PhotosListAdapter extends ArrayAdapter implements Serializable {

    private ArrayList<Photo> phList;
    private Activity ctx;

    public PhotosListAdapter(Activity context, ArrayList<Photo> phs){
        super(context, R.layout.photo_list_row_item, phs);
        ctx = context;
        phList = phs;
    }

    @Override

    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        LayoutInflater inflater = ctx.getLayoutInflater();
        if(convertView == null){
            row = inflater.inflate(R.layout.photo_list_row_item, null, true);
        }else{
            TextView fileLoc = (TextView) row.findViewById(R.id.FileLocation);
            ImageView iconView = (ImageView) row.findViewById(R.id.imageIcon);

            fileLoc.setText(phList.get(position).location);

            iconView.setImageBitmap(phList.get(position).currImg.getBitMapFormat());

        }
        return row;
    }

}
